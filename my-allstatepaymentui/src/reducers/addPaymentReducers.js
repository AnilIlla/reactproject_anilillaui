import {
  ADD_PAYMENT_DETAILS_BEGIN,
  ADD_PAYMENT_DETAILS_SUCCESS,
  ADD_PAYMENT_DETAILS_ERROR,
} from "../actionTypes";

const addPaymentReducer = (
  state = { isPaymentSaved: false, isPaymentError: false },
  action
) => {
  console.log(`received ${action.type} dispatch in add payment reducer`);
  switch (action.type) {
    case ADD_PAYMENT_DETAILS_SUCCESS:
      return {
        ...state,
        isPaymentSaved: true,
        isPaymentError: false,
      };
    case ADD_PAYMENT_DETAILS_ERROR:
      return { ...state, isPaymentError: true, isPaymentSaved: false };
    case ADD_PAYMENT_DETAILS_BEGIN:
    default:
      return state;
  }
};
export default addPaymentReducer;
