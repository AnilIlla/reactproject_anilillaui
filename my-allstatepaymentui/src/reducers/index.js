import { combineReducers } from "redux";
import paymentSearchReducer from "./paymentSearchReducer";
import paymentConnectionReducer from "./paymentConnectionReducer";
import addPaymentReducer from "./addPaymentReducers";

const rootReducer = combineReducers({
  fetchPayments: paymentSearchReducer,
  connection: paymentConnectionReducer,
  addPayment: addPaymentReducer,
});

export default rootReducer;
