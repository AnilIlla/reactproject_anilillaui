import {
  PAYMENT_CONNECTION_STATUS_FAILURE,
  PAYMENT_CONNECTION_STATUS_SUCCESS,
} from "../actionTypes";

const paymentConnectionReducer = (
  state = { connected: false, updating: true },
  action
) => {
  console.log(`received ${action.type} dispatch in payment connection reducer`);
  switch (action.type) {
    case PAYMENT_CONNECTION_STATUS_SUCCESS:
      return { ...state, connected: action.payload, updating: false };
    case PAYMENT_CONNECTION_STATUS_FAILURE:
      return { ...state, connected: action.payload, updating: false };
    default:
      return state;
  }
};
export default paymentConnectionReducer;
