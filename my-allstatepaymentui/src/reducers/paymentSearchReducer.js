import {
  FETCH_PAYMENT_DETAILS_BEGIN,
  FETCH_PAYMENT_DETAILS_SUCCESS,
  FETCH_PAYMENT_DETAILS_ERROR,
} from "../actionTypes";

const paymentSearchReducer = (state = { data: [] }, action) => {
  console.log(`received ${action.type} dispatch in search payment reducer`);
  switch (action.type) {
    case FETCH_PAYMENT_DETAILS_BEGIN:
      return { ...state, loading: true, error: null };
    case FETCH_PAYMENT_DETAILS_SUCCESS:
      return { ...state, data: action.payload, loading: false };
    case FETCH_PAYMENT_DETAILS_ERROR:
      return { ...state, data: [], loading: false, error: action.payload };
    default:
      return state;
  }
};
export default paymentSearchReducer;
