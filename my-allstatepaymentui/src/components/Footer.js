const Footer = () => {
  return (
    <footer className="footer mt-auto py-3">
      <div className="container">
        <span className="text-muted">
          <p>
            &copy; 2020-2021 Allstate Company, Inc. &middot;{" "}
            <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
          </p>
        </span>
      </div>
    </footer>
  );
};

export default Footer;
