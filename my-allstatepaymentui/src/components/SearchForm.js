import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { fetchPayments } from "../actions/PaymentSearchActions";

const SearchForm = () => {
  const [searchText, setSearchText] = useState("");
  const dispatch = useDispatch();
  const handleSearchSubmit = (event) => {
    event.preventDefault();
    dispatch(fetchPayments(searchText));
    return false;
  };

  return (
    <div>
      <h1> Payment Search</h1>
      <br></br>
      <form className="form-inline" onSubmit={handleSearchSubmit}>
        <div className="form-group mb-2">
          <label htmlFor="searchText">Enter Search Text </label>
        </div>
        <div className="form-group mx-sm-3 mb-2">
          <input
            type="text"
            className="form-control"
            id="searchText"
            value={searchText}
            onChange={(event) => setSearchText(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary mb-2">
          Search
        </button>
      </form>
    </div>
  );
};

export default SearchForm;
