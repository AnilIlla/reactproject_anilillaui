import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { checkPaymentConnection } from "../actions/PaymentConnectionAction";
const AboutUI = () => {
  const dispatch = useDispatch();
  const isConnected = useSelector((state) => state.connection.connected);
  const updating = useSelector((state) => state.connection.updating);
  useEffect(() => {
    dispatch(checkPaymentConnection());
  });

  if (updating) {
    return (
      <div className="row">
        <div className="col-lg-12">
          <div className="d-flex justify-content-center">
            <div
              className="spinner-border m-5"
              style={{ width: "4rem", height: "4rem" }}
              role="status"
            >
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="row">
      <div className="col-lg-12">
        {isConnected ? (
          <div className="alert alert-success fade show">
            <h1 className="alert-heading">
              <i className="fa fa-success"></i> Success!
            </h1>
            <h4>Allstate Payment Gateway Service is up and Running</h4>
          </div>
        ) : (
          <div className="alert alert-danger fade show">
            <h1 className="alert-heading">
              <i className="fa fa-danger"></i> Oops!
            </h1>
            <h4>
              Allstate Payment Gateway Service is down. Please try again after
              sometime...!
            </h4>
          </div>
        )}
      </div>
    </div>
  );
};
export default AboutUI;
