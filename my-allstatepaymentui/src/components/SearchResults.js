import React from "react";
import { useSelector } from "react-redux";

const SearchResults = () => {
  const payments = useSelector((state) => state.fetchPayments.data);
  const loading = useSelector((state) => state.fetchPayments.loading);
  const error = useSelector((state) => state.fetchPayments.error);

  if (error) {
    return (
      <div className="alert alert-warning fade show">
        <h4 className="alert-heading">
          <i className="fa fa-warning"></i> <h3>{error.message}</h3>
        </h4>
      </div>
    );
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div>
      {payments.map((payment, index) => (
        <div className="row mb-4" key={payment.id}>
          <div className="col-sm-3 themed-grid-col">{payment.id}</div>
          <div className="col-sm-3 themed-grid-col">{payment.type}</div>
          <div className="col-sm-3 themed-grid-col">{payment.amount}</div>
          <div className="col-sm-3 themed-grid-col">{payment.paymentdate}</div>
        </div>
      ))}
    </div>
  );
};

export default SearchResults;
