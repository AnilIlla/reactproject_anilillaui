import React from "react";
const Banner = () => {
  return (
    <div id="myCarousel" className="carousel slide" data-ride="carousel">
      <div className="carousel-inner">
        <div className="carousel-item active">
          <svg
            className="bd-placeholder-img"
            width="100%"
            height="100%"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="xMidYMid slice"
            focusable="false"
            role="img"
          >
            <rect width="100%" height="100%" fill="#777"></rect>
          </svg>
          <div className="container">
            <div className="carousel-caption text-center">
              Allstate Payment System
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Banner;
