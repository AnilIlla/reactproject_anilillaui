import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addPaymentDetails } from "../actions/AddPaymentActions";
import { useEffect } from "react";

const AddPayment = () => {
  const dispatch = useDispatch();
  const isSuccessfullySubmitted = useSelector(
    (state) => state.addPayment.isPaymentSaved
  );
  const isSubmitFailed = useSelector(
    (state) => state.addPayment.isPaymentError
  );
  const { register, handleSubmit, errors, formState, reset } = useForm({
    mode: "onBlur",
  });

  const handlePaymentSubmit = (data, event) => {
    event.preventDefault();
    dispatch(addPaymentDetails(data))
      .then((resolve) => {
        if (resolve) {
          reset();
        }
      })
      .catch((error) => {
        console.log("failed saving payment" + error);
      });
  };

  const validatePaymentDate = (paymentDate) => {
    if (paymentDate) {
      if (
        paymentDate.match(
          /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/
        )
      ) {
        console.log("regex matched");
        return true;
      } else {
        console.log("regx not matched");
        return false;
      }
    } else {
      console.log("empty");
      return false;
    }
  };

  return (
    <div className="row">
      <div className="col-lg-12">
        <h1>Add New Payment Details</h1>
        <br></br>
        {isSuccessfullySubmitted && (
          <div className="alert alert-success fade show">
            <div className="alert-heading">
              <i className="fa fa-success"></i> Payment Details submitted
              successfully
            </div>
          </div>
        )}
        {isSubmitFailed && (
          <div className="alert alert-warning fade show">
            <div className="alert-heading">
              <i className="fa fa-warning"></i> Payment Details could not be
              saved now.. Please try again later.
            </div>
          </div>
        )}
        <br></br>
        <form onSubmit={handleSubmit(handlePaymentSubmit)}>
          <div className="form-row">
            <div className="form-group col-md-3">
              <label htmlFor="inputPaymentId">Payment Id : </label>
              <input
                ref={register({ required: true, maxLength: 3 })}
                name="id"
                type="number"
                className="form-control"
                id="inputPaymentId"
                style={{ borderColor: errors.id && "red" }}
                placeholder="enter payment id"
              />
              <p style={{ color: "red" }}>
                {errors.id &&
                  errors.id.type === "required" &&
                  "Payment Id is required"}
                {errors.id &&
                  errors.id.type === "maxLength" &&
                  "Payment Id is cannot be more than 1000"}
              </p>
            </div>
            <div className="form-group col-md-3">
              <label htmlFor="paymentCustId">Customer Id : </label>
              <input
                ref={register({ required: true, maxLength: 3 })}
                name="custId"
                type="number"
                className="form-control"
                id="paymentCustId"
                style={{ borderColor: errors.custId && "red" }}
                placeholder="enter customer id"
              />
              <p style={{ color: "red" }}>
                {errors.custId &&
                  errors.custId.type === "required" &&
                  "Payment customer id is required"}
                {errors.custId &&
                  errors.custId.type === "maxLength" &&
                  "Payment customer Id is cannot be more than 1000"}
              </p>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-2">
              <label htmlFor="paymentAmount">Payment Amount :</label>
              <input
                name="amount"
                ref={register({ required: true })}
                type="number"
                step="any"
                className="form-control"
                id="paymentAmount"
                style={{ borderColor: errors.amount && "red" }}
                placeholder="enter payment anount"
              />
              <p style={{ color: "red" }}>
                {errors.amount &&
                  errors.amount.type === "required" &&
                  "Payment Id is required"}
              </p>
            </div>
            <div className="form-group col-md-2">
              <label htmlFor="paymentType">Payment Type :</label>
              <select
                name="type"
                ref={register({ required: true })}
                id="paymentType"
                className="form-control"
                style={{ borderColor: errors.type && "red" }}
                placeholder="enter payment type"
              >
                <option value="">Choose...</option>
                <option value="upi">UPI</option>
                <option value="net banking">Net Banking</option>
                <option value="phone pe">Phone pe</option>
                <option value="debit card">Debit Card</option>
                <option value="credit card">Credit Card</option>
              </select>
              <p style={{ color: "red" }}>
                {errors.type && "Payment type is required"}
              </p>
            </div>
            <div className="form-group col-md-2">
              <label htmlFor="paymentdate">Payment Date :</label>
              <input
                ref={register({
                  required: true,
                  validate: (input) => validatePaymentDate,
                })}
                name="paymentdate"
                type="date"
                className="form-control"
                id="paymentdate"
                style={{
                  borderColor: errors.paymentdate && "red",
                }}
              />
              <p style={{ color: "red" }}>
                {errors.paymentdate &&
                  errors.paymentdate.type === "required" &&
                  "Payment Date is Required"}
                {errors.paymentdate &&
                  errors.paymentdate.type === "validate" &&
                  "Enter a valid payment date"}
              </p>
            </div>
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={formState.isSubmitting}
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddPayment;
