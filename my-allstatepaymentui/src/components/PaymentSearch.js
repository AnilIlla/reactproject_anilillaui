import React from "react";
import SearchForm from "./SearchForm";
import SearchResults from "./SearchResults";

const PaymentSearch = (props) => {
  return (
    <div className="row">
      <div className="col-lg-12">
        <SearchForm />
        <SearchResults />
      </div>
    </div>
  );
};

export default PaymentSearch;
