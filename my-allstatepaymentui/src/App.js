import PaymentSearch from "./components/PaymentSearch";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import React from "react";
import Footer from "./components/Footer";
import Banner from "./components/Banner";
import AddPayment from "./components/AddPayment";
import AboutUI from "./components/AboutUI";

const App = () => {
  return (
    <Router>
      <Navbar />
      <main role="main" className="flex-shrink-0 h-100">
        <Banner />
        <div className="container marketing h-100">
          <Switch>
            <Route exact path="/">
              <PaymentSearch />
            </Route>
            <Route path="/add">
              <AddPayment />
            </Route>
            <Route path="/aboutapp">
              <AboutUI />
            </Route>
          </Switch>
        </div>
        <Footer />
      </main>
    </Router>
  );
};

export default App;
