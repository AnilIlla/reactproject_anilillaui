import axios from "axios";

import {
  PAYMENT_CONNECTION_STATUS_FAILURE,
  PAYMENT_CONNECTION_STATUS_SUCCESS,
} from "../actionTypes";

const paymentConnectionSuccess = (connected) => {
  return {
    type: PAYMENT_CONNECTION_STATUS_SUCCESS,
    payload: connected,
  };
};

const paymentConnectionFailure = (connected) => {
  return {
    type: PAYMENT_CONNECTION_STATUS_FAILURE,
    payload: connected,
  };
};

export const checkPaymentConnection = () => {
  return (dispatch, getState) => {
    console.log("state before connection :", getState());
    axios.get("http://localhost:8080/payment/gateway/status").then(
      (res) => {
        console.log(res);
        dispatch(paymentConnectionSuccess(true));
        console.log("state after paymentConnectionSuccess", getState());
      },
      (err) => {
        console.log(err);
        dispatch(paymentConnectionFailure(false));
        console.log("state after paymentConnectionFailure", getState());
      }
    );
  };
};
