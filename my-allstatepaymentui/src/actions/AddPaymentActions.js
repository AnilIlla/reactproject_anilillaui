import axios from "axios";

import {
  ADD_PAYMENT_DETAILS_BEGIN,
  ADD_PAYMENT_DETAILS_SUCCESS,
  ADD_PAYMENT_DETAILS_ERROR,
} from "../actionTypes";

const addPaymentDetailsBegin = () => {
  return {
    type: ADD_PAYMENT_DETAILS_BEGIN,
  };
};

const addPaymentDetailsSuccess = (success) => {
  return {
    type: ADD_PAYMENT_DETAILS_SUCCESS,
    payload: success,
  };
};

const addPaymentDetailsError = (error) => {
  return {
    type: ADD_PAYMENT_DETAILS_ERROR,
    payload: error,
  };
};

const ROOT_URL = "http://localhost:8080/payment/gateway/save";

export const addPaymentDetails = (payment) => {
  return async (dispatch, getState) => {
    dispatch(addPaymentDetailsBegin());
    console.log("state after addPaymentDetailsBegin", getState());
    return await axios.post(ROOT_URL, payment).then(
      (res) => {
        console.log(res);
        dispatch(addPaymentDetailsSuccess(true));
        console.log("state after addPaymentDetailsSuccess", getState());
        return getState().addPayment.isPaymentSaved;
      },
      (err) => {
        console.log(err);
        dispatch(addPaymentDetailsError(true));
        console.log("state after addPaymentDetailsError", getState());
        return getState().addPayment.isPaymentSaved;
      }
    );
  };
};
