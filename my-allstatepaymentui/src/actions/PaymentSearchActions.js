import axios from "axios";

import {
  FETCH_PAYMENT_DETAILS_BEGIN,
  FETCH_PAYMENT_DETAILS_SUCCESS,
  FETCH_PAYMENT_DETAILS_ERROR,
} from "../actionTypes";

const fetchPaymentDetailsBegin = () => {
  return {
    type: FETCH_PAYMENT_DETAILS_BEGIN,
  };
};

const fetchPaymentDetailsSuccess = (payments) => {
  return {
    type: FETCH_PAYMENT_DETAILS_SUCCESS,
    payload: payments,
  };
};

const fetchPaymentDetailsError = (error) => {
  return {
    type: FETCH_PAYMENT_DETAILS_ERROR,
    payload: error,
  };
};

const ROOT_URL = "http://localhost:8080/payment/gateway/";

export const fetchPayments = (searchText) => {
  return (dispatch, getState) => {
    if (isNaN(searchText)) {
      searchText = "type/" + searchText;
    } else {
      searchText = "id/" + searchText;
    }
    dispatch(fetchPaymentDetailsBegin());
    axios.get(ROOT_URL + searchText).then(
      (res) => {
        console.log(res);
        if (res.data instanceof Array) {
          dispatch(fetchPaymentDetailsSuccess(res.data));
        } else {
          dispatch(fetchPaymentDetailsSuccess([res.data]));
        }
        console.log("state after fetchPaymentDetailsSuccess", getState());
      },
      (err) => {
        let error;
        if (err.response) {
          error = err.response.data;
        } else {
          error = {
            message: "Failed to fetch payments.. please try again later",
          };
        }
        dispatch(fetchPaymentDetailsError(error));
        console.log("state after fetchPaymentDetailsError", getState());
      }
    );
  };
};
